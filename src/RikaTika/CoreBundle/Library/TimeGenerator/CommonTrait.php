<?php

namespace RikaTika\CoreBundle\Library\TimeGenerator;

/**
 * CommonTrait
 *
 * @category RikaTika
 * @package CoreBundle
 * @subpackage Library\TimeGenerator
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2014, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
trait CommonTrait
{
    /**
     * Generate a random hour based on constraints.
     *
     * @return int
     */
    protected static function getHoursRandom()
    {
        return self::getRandomArrayItem(self::getHoursAvailable());
    }

    /**
     * Generate a random minutes based on constraints.
     *
     * @return int
     */
    protected static function getMinutesRandom()
    {
        return self::getRandomArrayItem(self::getMinutesAvailable());
    }

    /**
     * Get random array item from array.
     *
     * @param array $a
     * @return int
     */
    protected static function getRandomArrayItem(array $a)
    {
        $max = count($a);

        return (0 < $max) ? $a[mt_rand(0, --$max)] : 0;
    }

    /**
     * Increment or decrement the time by a number of hours.
     *
     * @param int $addend
     * @return TimeGeneratorAbstract
     */
    public function deltaHours($addend = 0)
    {
        $addend *= self::H_SECONDS;

        return $this->deltaSeconds($addend);
    }

    /**
     * Increment or decrement the time by a number of minutes.
     *
     * @param int $addend
     * @return TimeGeneratorAbstract
     */
    public function deltaMinutes($addend = 0)
    {
        $addend *= self::M_SECONDS;

        return $this->deltaSeconds($addend);
    }

    /**
     * Increment or decrement the time by a number of minutes.
     *
     * @param int $addend
     * @return TimeGeneratorAbstract
     */
    public function deltaSeconds($addend = 0)
    {
        $hMax = self::getHourMax() - (TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour() ? 0 : 1);
        $mMax = self::getMinuteMax();
        $sMax = self::getSecondMax();

        $timeMax =
            $hMax * self::H_SECONDS +
            $mMax * self::M_SECONDS +
            $sMax + 1;

        if (0 !== $addend) {
            $addend %= $timeMax;
        }

        $time = self::getTimeInSeconds() + $addend;

        if ($time < 0) {
            $time += $timeMax;
        } elseif ($timeMax <= $time) {
            $time -= $timeMax;
        }

        $this->setTimeInSeconds($time);

        return $this;
    }

    /**
     * Multiply hours by a certain factor.
     *
     * @param int $factor
     * @return TimeGeneratorAbstract
     */
    public function multiplyHours($factor = 1)
    {
        $hour  = $this->getHour();
        $hour *= $factor;

        $this->setHour($hour);

        return $this;
    }

    abstract public function toText();

    /**
     * Convert time in hour and minute (and second) to a formatted time string.
     *
     * @return string
     */
    public function toTime()
    {
        $h = $this->toHour();
        $m = ':' . $this->toMinute();
        $s = self::getExerciseSettings()->getHasSeconds() ? ':' . $this->toSecond() : '';

        return $h . $m . $s;
    }

    /**
     * Convert the hour component of time to a formatted string.
     *
     * @return string
     */
    protected function toHour()
    {
        $h = $this->getHour();

        // @todo move this functionality to setHasTwentyFour(false);
        if (!self::getModuleSettings()->getHasTwentyFour()) {
            $h = $this->to12HourClock($h);
        }

        return $this->toDigit($h);
    }

    /**
     * Convert the minute component of time to a formatted string.
     *
     * @return string
     */
    protected function toMinute()
    {
        $m = $this->getMinute();

        return $this->toDigit($m);
    }

    /**
     * Convert the second component of time to a formatted string.
     *
     * @return string
     */
    protected function toSecond()
    {
        $s = $this->getSecond();

        return $this->toDigit($s);
    }

    /**
     * Convert integer to a formatted string.
     *
     * @param $digit
     * @return string
     */
    protected function toDigit($digit)
    {
        return sprintf('%02d', $digit);
    }

    /**
     * Convert hour to 12-hour clock format (01:00:00 to 12:59:59)
     *
     * @param int $h
     * @return int
     */
    public function to12HourClock($h)
    {
        if (0 === $h) {
            $h = 12;
        } elseif (12 < $h) {
            $h -= 12;
        }

        return $h;
    }

    /**
     * Convert hour to 24-hour clock A.M.
     * @todo Confirm Ante Meridiem is 00:00:00 to 11:59:59
     *
     * @param int $h
     * @return int
     */
    public function to24HourClockAnteMeridiem($h)
    {
        if (12 <= $h) {
            $h -= 12;
        }

        return $h;
    }

    /**
     * Convert hour to 24-hour P.M.
     * @todo Confirm Post Medidiem is 12:00:00 to 23:59:59
     *
     * @param int $h
     * @return int
     */
    public function to24HourClockPostMeridiem($h)
    {
        if ($h < 12) {
            $h += 12;
        }

        return $h;
    }

    /**
     * Get available hours according to Module Settings.
     *
     * @return array
     */
    public function getHoursAvailable()
    {
        list ($hMin, $hMax) = self::getHourRange();
        for ($hours = [], $i = $hMin; $i <= $hMax; ++$i) {
            $hours[] = $i;
        }

        return $hours;
    }

    /**
     * Get available minutes according to Module Settings.
     *
     * @return array
     */
    public function getMinutesAvailable()
    {
        $exerciseSettings = self::getExerciseSettings();

        $minutes = [];

        if ($exerciseSettings->getHasFullHour() ) {
            $minutes[] = 0;
        }

        if ($exerciseSettings->getHasQuarterPast() ) {
            $minutes[] = 15;
        }

        if ($exerciseSettings->getHasHalfHour() ) {
            $minutes[] = 30;
        }

        if ($exerciseSettings->getHasQuarterTo() ) {
            $minutes[] = 45;
        }

        if (0 < $exerciseSettings->getMinutesIncrement()) {
            $step = $exerciseSettings->getMinutesIncrement();
            $minutes = array_unique(array_merge(range(0, 59, $step), $minutes));
            sort($minutes);
        }

        return $minutes;
    }
}
