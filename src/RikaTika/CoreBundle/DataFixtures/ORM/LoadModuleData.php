<?php

namespace RikaTika\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use RikaTika\CoreBundle\Entity\Module;

/**
 * LoadModuleData
 *
 * @category RikaTika
 * @package CoreBundle
 * @subpackage DataFixtures\ORM
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2014, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class LoadModuleData extends AbstractFixture
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $moduleA = new Module();
        $moduleA
            ->setName('Module clock knowledge')
            ->setIsActive(false);
        $entityManager->persist($moduleA); // Manage Entity for persistence.
        $this->addReference('moduleA', $moduleA); // Reference for the next Data Fixture(s).

        $moduleB = new Module();
        $moduleB
            ->setName('Module clock reading')
            ->setIsActive(true);
        $entityManager->persist($moduleB); // Manage Entity for persistence.
        $this->addReference('moduleB', $moduleB); // Reference for the next Data Fixture(s).

        $moduleC = new Module();
        $moduleC
            ->setName('Module setting time')
            ->setIsActive(true);
        $entityManager->persist($moduleC); // Manage Entity for persistence.
        $this->addReference('moduleC', $moduleC); // Reference for the next Data Fixture(s).

        $moduleD = new Module();
        $moduleD
            ->setName('Module transposing clocks')
            ->setIsActive(true);
        $entityManager->persist($moduleD); // Manage Entity for persistence.
        $this->addReference('moduleD', $moduleD); // Reference for the next Data Fixture(s).

        $moduleE = new Module();
        $moduleE
            ->setName('Module interpreting clocks')
            ->setIsActive(false);
        $entityManager->persist($moduleE); // Manage Entity for persistence.
        $this->addReference('moduleE', $moduleE); // Reference for the next Data Fixture(s).

        $entityManager->flush(); // Persist all managed objects.
    }
}
