<?php

namespace RikaTika\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use RikaTika\CoreBundle\Entity\Language;

/**
 * LoadLanguageData
 *
 * @category RikaTika
 * @package CoreBundle
 * @subpackage DataFixtures\ORM
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2014, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class LoadLanguageData extends AbstractFixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $languageEN = new Language();
        $languageEN->setCode('EN');
        $entityManager->persist($languageEN); // Manage Entity for persistence.
        $this->addReference('languageEN', $languageEN); // Reference for the next Data Fixture(s).

        $languageNL = new Language();
        $languageNL->setCode('NL');
        $entityManager->persist($languageNL); // Manage Entity for persistence.
        $this->addReference('languageNL', $languageNL); // Reference for the next Data Fixture(s).

        $entityManager->flush(); // Persist all managed objects.
    }
}
