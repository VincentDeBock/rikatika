<?php

namespace RikaTika\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SupervisorType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('profession')
            ->add('exerciseSettings')
            ->add('moduleSettings')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RikaTika\CoreBundle\Entity\Supervisor'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rikatika_corebundle_supervisor';
    }
}
