'use strict';

var gulp   = require('gulp'),
    bower  = require('gulp-bower'),
    notify = require('gulp-notify'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    sass   = require('gulp-ruby-sass'),

// Tasks
// -----
    /**
     * Run all tasks when 'gulp' is executed.
     */
    gulp.task('default', ['css', 'js'], function() {
    // Default task must exist.
});

/**
 * Install Bower packages defined in bower.json into the given components folder.
 */
gulp.task('bower', function() {
    // …
});

/**
 * Create font files in destination folder.
 */
gulp.task('fonts', ['bower'], function() {
    // …
});

/**
 * Create CSS files in destination folder.
 */
gulp.task('css', ['bower', 'fonts'], function() {
    // …
});

/**
 * Create JS files in destination folder.
 */
gulp.task('js', ['bower'], function() {
    // …
});