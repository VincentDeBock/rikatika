<?php

namespace RikaTika\CoreBundle\Entity\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Security\Core\Encoder\EncoderFactory;

use RikaTika\CoreBundle\Entity\UserAbstract;

/**
 * UserListener
 *
 * @category RikaTika
 * @package CoreBundle
 * @subpackage Entity\Listener
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2014, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class UserListener
{
    /**
     * @var EncoderFactory
     */
    private $encoderFactory;

    /**
     * @param EncoderFactory $encoderFactory
     */
    public function __construct(EncoderFactory $encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
    }

    /**
     * @param UserAbstract $user
     * @param LifecycleEventArgs $event
     *
     * @ORM\PrePersist
     */
    public function prePersist(UserAbstract $user, LifecycleEventArgs $event)
    {
        $encoder = $this->encoderFactory->getEncoder($user);
        $passwordHashed = $encoder->encodePassword($user->getPassword(), $user->getSalt());
        $user->setPassword($passwordHashed);
    }

}
