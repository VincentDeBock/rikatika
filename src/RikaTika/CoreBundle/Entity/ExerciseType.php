<?php

namespace RikaTika\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExerciseType
 *
 * @category RikaTika
 * @package CoreBundle
 * @subpackage Entity
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2014, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 *
 * @ORM\Table(name="exercise_types")
 * @ORM\Entity(repositoryClass="RikaTika\CoreBundle\Entity\ExerciseTypeRepository")
 */
class ExerciseType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Module
     *
     * @ORM\ManyToOne(targetEntity="Module")
     * @ORM\JoinColumn(nullable=false)
     */
    private $module;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_digital", type="boolean")
     */
    private $isDigital;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set module
     *
     * @param Module $module
     * @return ExerciseType
     */
    public function setModule(Module $module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return Module
     */
    public function getModule()
    {
        return $this->module->getName();
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ExerciseType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isDigital
     *
     * @param boolean $isDigital
     * @return ExerciseType
     */
    public function setIsDigital($isDigital)
    {
        $this->isDigital = $isDigital;

        return $this;
    }

    /**
     * Get isDigital
     *
     * @return boolean 
     */
    public function getIsDigital()
    {
        return $this->isDigital;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return ExerciseType
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }



}
