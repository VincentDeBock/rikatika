<?php

namespace RikaTika\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Exercise
 *
 * @category RikaTika
 * @package CoreBundle
 * @subpackage Entity
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2014, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 *
 * @ORM\Table(name="exercises")
 * @ORM\Entity
 */
class Exercise
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ExerciseType
     *
     * @ORM\ManyToOne(targetEntity="ExerciseType")
     * @ORM\JoinColumn(name="exercise_type_id", nullable=false)
     */
    private $exerciseType;

    /**
     * @var ExerciseSet
     *
     * @ORM\ManyToOne(targetEntity="ExerciseSet")
     * @ORM\JoinColumn(name="exercise_set_id", nullable=false)
     */
    private $exerciseSet;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var array
     *
     * @ORM\Column(name="questions", type="array")
     */
    private $questions;

    /**
     * @var array
     *
     * @ORM\Column(name="answers", type="array")
     */
    private $answers;

    /**
     * @var array
     *
     * @ORM\Column(name="modes", type="array")
     */
    private $modes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_correct", type="boolean")
     */
    private $isCorrect;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="started_at", type="datetime")
     */
    private $startedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ended_at", type="datetime")
     */
    private $endedAt;



    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set exercise type
     *
     * @param ExerciseType $exerciseType
     * @return Exercise
     */
    public function setExerciseType(ExerciseType $exerciseType)
    {
        $this->exerciseType = $exerciseType;

        return $this;
    }

    /**
     * Get exercise set
     *
     * @return ExerciseType
     */
    public function getExerciseType()
    {
        return $this->exerciseType->getName();
    }

    /**
     * Set exercise set
     *
     * @param ExerciseSet $exerciseSet
     * @return Exercise
     */
    public function setExerciseSet(ExerciseSet $exerciseSet)
    {
        $this->exerciseSet = $exerciseSet;

        return $this;
    }

    /**
     * Get exercise set
     *
     * @return ExerciseSet
     */
    public function getExerciseSet()
    {
        return $this->exerciseSet;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Exercise
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set questions
     *
     * @param array $questions
     * @return Exercise
     */
    public function setQuestions($questions)
    {
        $this->questions = $questions;

        return $this;
    }

    /**
     * Get questions
     *
     * @return array
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Set answers
     *
     * @param array $answers
     * @return Exercise
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;

        return $this;
    }

    /**
     * Get answers
     *
     * @return array 
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Set modes
     *
     * @param array $modes
     * @return Exercise
     */
    public function setModes($modes)
    {
        $this->modes = $modes;

        return $this;
    }

    /**
     * Get modes
     *
     * @return array 
     */
    public function getModes()
    {
        return $this->modes;
    }

    /**
     * Set isCorrect
     *
     * @param boolean $isCorrect
     * @return Exercise
     */
    public function setIsCorrect($isCorrect)
    {
        $this->isCorrect = $isCorrect;

        return $this;
    }

    /**
     * Get isCorrect
     *
     * @return boolean 
     */
    public function getIsCorrect()
    {
        return $this->isCorrect;
    }

    /**
     * Set startedAt
     *
     * @param \DateTime $startedAt
     * @return Exercise
     */
    public function setStartedAt($startedAt)
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    /**
     * Get startedAt
     *
     * @return \DateTime 
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }

    /**
     * Set endedAt
     *
     * @param \DateTime $endedAt
     * @return Exercise
     */
    public function setEndedAt($endedAt)
    {
        $this->endedAt = $endedAt;

        return $this;
    }

    /**
     * Get endedAt
     *
     * @return \DateTime 
     */
    public function getEndedAt()
    {
        return $this->endedAt;
    }
}
