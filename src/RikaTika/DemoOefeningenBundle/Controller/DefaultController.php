<?php

namespace RikaTika\DemoOefeningenBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('RikaTikaDemoOefeningenBundle:Default:index.html.twig', array('name' => $name));
    }
}
