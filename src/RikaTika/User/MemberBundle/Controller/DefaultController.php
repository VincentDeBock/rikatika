<?php

namespace RikaTika\User\MemberBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{

    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();


        $userset = $this->getDoctrine()
            ->getRepository('RikaTikaCoreBundle:ExerciseSet')
            ->findBy(
                array('member' => $user)
            );
        $exercisesinset = $this->getDoctrine()
            ->getRepository('RikaTikaCoreBundle:Exercise')
            ->findBy(
                array('exerciseSet' => $userset)
            );
        $exerciseattempts = $this->getDoctrine()
            ->getRepository('RikaTikaCoreBundle:ExerciseAttempt')
            ->findBy(
                array('exercise' =>$exercisesinset)
            );
        $correctexerciseattempts = $this->getDoctrine()
            ->getRepository('RikaTikaCoreBundle:ExerciseAttempt')
            ->findBy(
                array('exercise' =>$exercisesinset, 'isCorrect' =>1)
            );
        $falseexerciseattempts = $this->getDoctrine()
            ->getRepository('RikaTikaCoreBundle:ExerciseAttempt')
            ->findBy(
                array('exercise' =>$exercisesinset, 'isCorrect' =>0)
            );

        return $this->render(
            'RikaTikaUserMemberBundle:User:dashboard.html.twig',
            array('user' => $user,
                'exercises' => $exercisesinset,
                'usersets' => $userset,
                'exerciseattempts' => $exerciseattempts,
                'correctexerciseattempts' => $correctexerciseattempts,
                'falseexerciseattempts' => $falseexerciseattempts)
        );
    }

    /**
     * @Route("/intro")
     * @Template()
     */
    public function IntroAction()
    {
        return $this->render(
            'RikaTikaUserMemberBundle:Default:index.html.twig');
    }
}
