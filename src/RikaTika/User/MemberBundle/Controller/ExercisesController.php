<?php

namespace RikaTika\User\MemberBundle\Controller;

use RikaTika\CoreBundle\Entity\ExerciseAttempt;
use RikaTika\CoreBundle\Entity\ExerciseSet;
use RikaTika\CoreBundle\Entity\Exercise;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ExercisesController extends Controller
{
    /**
     * @Route("/exercises/")
     */
    public function indexAction()
    {
        $module = $this->getDoctrine()
            ->getRepository('RikaTikaCoreBundle:Module')
            ->findBy(
                array('isActive'=> '1')
            );

        $exercisetypes = $this->getDoctrine()
            ->getRepository('RikaTikaCoreBundle:ExerciseType')
            ->findAll();

        $exercise= $this->getDoctrine()
            ->getRepository('RikaTikaCoreBundle:Exercise')
            ->findAll();

        return $this->render(
            'RikaTikaUserMemberBundle:Exercises:index.html.twig',
            array('modules' => $module,
                'types' => $exercisetypes,
                'exercises' => $exercise)
        );


    }
    /**
     * @Route("/exercises/{id}/")
     * @Method("GET")
     */
    public function getExercise($id, Request $request)
    {

        //oefening ophalen met gekregen id
        $em = $this->getDoctrine()->getManager();
        $exerciseSet = $em->getRepository('RikaTikaCoreBundle:ExerciseSet')->findOneBy(
            array('member' => $this->getUser())
        );

        //geen set, maak een nieuwe set aan
        if(!$exerciseSet){
            //create exercise set
            $exerciseSet = new ExerciseSet();
            $exerciseSet->setMember($this->getUser());
            $exerciseSet->setCreatedAt(new \DateTime('now'));
            $em->persist($exerciseSet);
            $em->flush();

        }

        //als er een onafgewerkte oefening is, haal die op
        $uncompletedExercise = $em->getRepository('RikaTikaCoreBundle:Exercise')->findOneBy(
            array('isCorrect' => 0, 'exerciseSet' => $exerciseSet)
        );


        if(!$uncompletedExercise){
            //start een nieuwe oefening als er geen onafgewerkte is

            //random klok genereren
            $hour = rand(0, 11);
            $minute = rand(0, 59);
            $second = rand(0, 59);

            //klokdata in array steken
            $exerciseView=['hour' => $hour,
                'minute' => $minute,
                'second' => $second];
            $startHour = new \DateTime('now');


            //nieuwe oefening aanmaken

            $exercise = new Exercise();
            $exercise->setQuestions($exerciseView);
            $exercise->setStartedAt($startHour);
            $exercise->setAnswers(null);
            $exercise->setEndedAt(new \DateTime('now'));
            $exercise->setExerciseSet($exerciseSet);
            $exercise->setExerciseType($this->getDoctrine()->getRepository('RikaTikaCoreBundle:ExerciseType')->find($id));
            $exercise->setIsCorrect(false);


            $em = $this->getDoctrine()->getManager();

            $em->persist($exercise);
            $em->flush();

            //exerise attempt maken
            $exerciseattempt = new ExerciseAttempt();

            //form genereren om oefening te submitten
            $form = $this->createFormBuilder($exerciseattempt)
                ->add('answer', 'text', array(
                    'attr'=> array(
                        'class'=>'hidemenow'
                    )
                ))
                ->add('save', 'submit', array(
                    'label' => 'Controleer',
                    'attr' => array(
                        'class' => 'submitbtn',
                    )))
                ->getForm();
            $form->handleRequest($request);

            //haal specifieke oefening template op
            $returnview = $this->render(
                'RikaTikaUserMemberBundle:Exercises:exercises'.$id.'.html.twig',
                array('exercise' => $exerciseView,
                    'exerciseid' => $exercise->getId(),
                    'id' => $id,
                    'form' =>$form->createView())
            );
        }
        else{
            //toon onafgewerkte oefening als die er is
            $oef = $uncompletedExercise->getId();

            //geef deze onafgewerkte oefening weer
            $returnview= $this->redirect($this->generateUrl("rikatika_user_member_exercises_getsameexercise",
                array('id' => $id,
                    'oef'=>$oef)));
        }



        //return correct view (completed or new exercise)
        return $returnview;

    }
    /**
     * @Route("/exercises/{id}/{oef}")
     * @Method("POST")
     */
    public function controlAction($id, $oef, Request $request) {

        //vraag alle oefeningen op
        $em = $this->getDoctrine()->getManager();
        $controlExercise = $em->getRepository('RikaTikaCoreBundle:Exercise')->findOneById($oef);

        //krijg de ingevulde antwoorden
        $getAnswer[] =  $request->request->All();
        $filledInAnswer = $getAnswer[0]["form"]["answer"];
        //krijg de juiste antwoorden
        $correctAnswerArray = $controlExercise->getQuestions();
        $correctAnswer = $correctAnswerArray["hour"] . ":" . $correctAnswerArray["minute"];

        //antwoord poging
        $exerciseattempt = new ExerciseAttempt();

        $exerciseattempt->setExercise($controlExercise);
        $exerciseattempt->setAnswer($filledInAnswer);
        $exerciseattempt->setEndedAt(new \DateTime('now'));
        $exerciseattempt->setMode("test");


        //verelijk ingevuld met correcte antwoord
        if($correctAnswer == $filledInAnswer){
            //exercise attempt op true zetten
            $correct = true;
            //oefenign op true: antwoord naar antworod, ended at op huidige tijd zetten
            $controlExercise->setAnswers($filledInAnswer);
            $controlExercise->setIsCorrect(true);
            $controlExercise->setEndedAt(new \DateTime('now'));
            $em->flush();

            //nieuwe oefening voor user
            $returnpath= $this->redirect($this->generateUrl("rikatika_user_member_exercises_getexercise",
                array('id' => $id)));
        }
        else{
            //
            $correct = false;

            //
            $returnpath= $this->redirect($this->generateUrl("rikatika_user_member_exercises_getsameexercise",
                array('id' => $id,
                    'oef'=>$oef)));

        }
        //oefening juist of fals zetten
        $exerciseattempt->setIsCorrect($correct);
        //create attempt
        $em->persist($exerciseattempt);
        $em->flush();


        return $returnpath;
    }

    /**
     * @Route("/exercises/{id}/{oef}")
     * @Method("GET")
     */
    public function getSameExercise($id, $oef, Request $request)
    {
        //oefeningen set ophalen voor user
        $em = $this->getDoctrine()->getManager();
        $exerciseSet = $em->getRepository('RikaTikaCoreBundle:ExerciseSet')->findOneBy(
            array('member' => $this->getUser())
        );

        //is er geen set dan een aanmaken
        if(!$exerciseSet){
            //create exercise set
            $exerciseSet = new ExerciseSet();
            $exerciseSet->setMember($this->getUser());
            $exerciseSet->setCreatedAt(new \DateTime('now'));
            $em->persist($exerciseSet);
            $em->flush();

        }


        //haal de vorige oefening op
        $previousExercise = $em->getRepository('RikaTikaCoreBundle:Exercise')->findOneById($oef);
        $previousQuestionArray = $previousExercise->getQuestions();
        $previousQuestionHour = $previousQuestionArray['hour'];
        $previousQuestionMinute = $previousQuestionArray['minute'];
        $previousQuestionSecond = $previousQuestionArray['second'];




        $exerciseView=['hour' => $previousQuestionHour,
            'minute' => $previousQuestionMinute,
            'second' => $previousQuestionSecond];


        $exerciseattempt = new ExerciseAttempt();

        $form = $this->createFormBuilder($exerciseattempt)
            ->add('answer', 'text')
            ->add('save', 'submit', array(
                'label' => 'Controleer',
                'attr' => array(
                    'class' => 'submitbtn',
                )))
            ->getForm();
        $form->handleRequest($request);


        return $this->render(
            'RikaTikaUserMemberBundle:Exercises:exercises'.$id.'.html.twig',
            array('exercise' => $exerciseView,
                'exerciseid' => $oef,
                'id' => $id,
                'form' =>$form->createView())
        );
    }


}
