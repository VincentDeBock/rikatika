<?php

namespace RikaTika\User\MemberBundle\Controller;

use Proxies\__CG__\RikaTika\CoreBundle\Entity\MemberGroup;
use RikaTika\CoreBundle\Entity\UserAbstract;
use RikaTika\CoreBundle\Entity\Member;
use RikaTika\CoreBundle\Form\UserAbstractType;
use RikaTika\User\MemberBundle\Form\MemberLoginType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


class UserController extends Controller
{
    /**
     * @Route("/login")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function loginAction(Request $request)
    {
        $entity   = new Member();
        $formType = new MemberLoginType();

        $form = $this->createForm($formType, $entity, [
            'action' => $this->generateUrl('rikatika_user_member_user_check')
        ]);

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $session = $request->getSession();
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return [
            'form'  => $form->createView(),
            'error' => $error,
        ]; // Return array with variables for Twig.
    }


    /**
     * @Route("/signup")
     */
    public function signupAction(Request $request)
    {
        if($request->getMethod() == 'POST'){
            $username=$request->get('username');
            $firstname=$request->get('firstname');
            $lastname=$request->get('lastname');
            $password=$request->get('password');
            $language=$request->get('language');
            //$member_group = '1';

            $member = new Member();
            $member->setUsername($username);
            $member->setFirstName($firstname);
            $member->setLastName($lastname);
            $member->setPassword($password);
            $member->setBirthday(new \DateTime('now'));



            $em = $this->getDoctrine()->getManager();
            $variabele = $em->getRepository('RikaTikaCoreBundle:Language')->find($language);
            $member->setLanguage($variabele);

            $em=$this->getDoctrine()->getEntityManager();
            $em->persist($member);
            $em->flush();

            $url = $this->generateUrl('rikatika_user_all_default_index');
            return $this->redirect($url);

        }
        //return $this->redirectToRoute('rikatika_user_member_user_login');
        Return $this->render('RikaTikaUserMemberBundle:User:signup.html.twig');
        //Return $this->render("RikaTikaUserMemberBundle:User:signup.html.twig");
    }





}
