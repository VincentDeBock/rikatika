<?php

namespace RikaTika\User\MemberBundle\Form;

use RikaTika\CoreBundle\Form\MemberType;
use Symfony\Component\Form\FormBuilderInterface;

class MemberLoginType extends MemberType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username','text',array(
                'label_attr' => array('class' => 'f200 displayblock')
            ))
            ->add('password', 'password',array(
                'label_attr' => array('class' => 'f200 displayblock')
            ))
            ->add('btn_login', 'submit', [
                'label' => 'Sign in',
                'attr' => array('class' =>'btn btn-sign-form')
            ])

        ;
    }

    /**
     * Form name.
     *
     * @return string
     */
    public function getName()
    {
        return 'login';
    }
} 
