<?php

namespace RikaTika\User\AdministratorBundle\Controller;

use RikaTika\CoreBundle\Entity\Administrator;
use RikaTika\User\AdministratorBundle\Form\AdministratorLoginType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class UserController extends Controller
{
    /**
     * @Route("/login")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function loginAction(Request $request)
    {
        $entity   = new Administrator();
        $formType = new AdministratorLoginType();

        $form = $this->createForm($formType, $entity, [
            'action' => $this->generateUrl('rikatika_user_administrator_user_check')
        ]);

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $session = $request->getSession();
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return [
            'form'  => $form->createView(),
            'error' => $error,
        ]; // Return array with variables for Twig.
    }

    /**
     * @Route("/intro")
     * @Template()
     */
    public function introAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('RikaTikaCoreBundle:UserAbstract');

        $users = $repository->findAll();

        return array('users' => $users);
    }

    /**
     * @Route("/excercises")
     * @Template()
     */
    public function excercisesAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('RikaTikaCoreBundle:ExerciseType');

        $excercises = $repository->findAll();

        return array('excercises' => $excercises);
    }

    /**
     * @Route("/excercises/delete/{id}")
     * @Template()
     */
    public function deleteAction($id)
    {
        if ($id == 0){ // no user id entered
            return $this->redirect($this->generateUrl('rikatika_user_administrator_user_excercises'), 301);
        }

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('RikaTikaCoreBundle:ExerciseType')->find($id);

        if (!$user) { // no user in the system
            throw $this->createNotFoundException(
                'No ExerciseType found for id '.$id
            );
        } else {
            $em->remove($user);
            $em->flush();
            return $this->redirect($this->generateUrl('rikatika_user_administrator_user_excercises'), 301);
        }
    }


    /**
     * @Route("/modules")
     * @Template()
     */
    public function modulesAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('RikaTikaCoreBundle:Module');

        $modules = $repository->findAll();

        return array('modules' => $modules);
    }

    /**
     * @Route("/modules/deletem/{id}")
     * @Template()
     */
    public function deletemoduleAction($id)
    {
        if ($id == 0){ // no user id entered
            return $this->redirect($this->generateUrl('rikatika_user_administrator_user_modules'), 301);
        }

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('RikaTikaCoreBundle:Module')->find($id);

        if (!$user) { // no user in the system
            throw $this->createNotFoundException(
                'No module found for id '.$id
            );
        } else {
            $em->remove($user);
            $em->flush();
            return $this->redirect($this->generateUrl('rikatika_user_administrator_user_modules'), 301);
        }
    }

}
