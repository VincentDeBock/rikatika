<?php

namespace RikaTika\User\AdministratorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/index")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('RikaTikaCoreBundle:UserAbstract');

        $users = $repository->findAll();

        return array('users' => $users);
    }




    /**
     * @Route("/users/delete/{id}")
     * @Template()
     */
    public function deleteAction($id)
    {
        if ($id == 0){ // no user id entered
            return $this->redirect($this->generateUrl('rikatika_user_administrator_default_index'), 301);
        }

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('RikaTikaCoreBundle:UserAbstract')->find($id);

        if (!$user) { // no user in the system
            throw $this->createNotFoundException(
                'No user found for id '.$id
            );
        } else {
            $em->remove($user);
            $em->flush();
            return $this->redirect($this->generateUrl('rikatika_user_administrator_default_index'), 301);
        }
    }

}
