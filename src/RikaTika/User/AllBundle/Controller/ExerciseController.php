<?php

namespace RikaTika\User\AllBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ExerciseController extends Controller
{
    /**
     * @Route("/exercise")
     */
    public function indexAction()
    {
        $module = $this->getDoctrine()
            ->getRepository('RikaTikaCoreBundle:Module')
            ->findBy(
                array('isActive'=> '1')
            );


        $exercisetypes = $this->getDoctrine()
            ->getRepository('RikaTikaCoreBundle:ExerciseType')
            ->findAll();

        $exercise= $this->getDoctrine()
            ->getRepository('RikaTikaCoreBundle:Exercise')
            ->findAll();



        return $this->render(
            'RikaTikaUserAllBundle:Exercise:index.html.twig',
            array('modules' => $module,
                'types' => $exercisetypes,
                'exercises' => $exercise)
        );


    }
    /**
     * @Route("/exercise/{id}")
     */
    public function getExercise($id)
    {
        return $this->render(
            'RikaTikaUserAllBundle:Exercise:exercise'.$id.'.html.twig'
        );
    }



}
